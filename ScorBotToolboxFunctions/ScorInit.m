function confirm = ScorInit()
% SCORINIT initialize ScorBot
%   Check communication with scorbotserver
%   USB communication, and enabling control.
%
%   confirm = SCORINIT(___) returns 1 if successful and 0 otherwise.
%
%   See also: ScorHome ScorSafeShutdown
%
%   References:
%       [1] C. Wick, J. Esposito, & K. Knowles, US Naval Academy, 2010
%           http://www.usna.edu/Users/weapsys/esposito-old/_files/scorbot.matlab/MTIS.zip
%           Original function name "ScorInit.m"
%       
%   C. Wick, J. Esposito, K. Knowles, & M. Kutzer, 10Aug2015, USNA

% Updates
%   25Aug2015 - Updated correct help documentation, "J. Esposito K. 
%               Knowles," to "J. Esposito, & K. Knowles,"
%               Erik Hoss
%   19Jun2017 - Convert to scorbot server J.Donnal

%% Delete extra shutdown figure handles
ShutdownFig = 1845;
if ishandle(ShutdownFig)
    delete(ShutdownFig);
end

%% Create background figure to force ScorSafeShutdown when closing MATLAB
ShutdownFig = figure(ShutdownFig);
set(ShutdownFig,...
    'Color',[0.5,0.5,0.5],...
    'Name','ScorSafeShutdown',...
    'MenuBar','none',...
    'NumberTitle','off',...
    'ToolBar','none',...
    'Units','normalized',...
    'Position',[0.005,0.943,0.167,0.028],...
    'HandleVisibility','off',...
    'Visible','off',...
    'CloseRequestFcn',@ScorShutdownCallback);

%% Check if scorbot server is available
status = ScorServerCmd('RIsInitDone');
if(status=='ERROR')
    fprintf('Error communicating with ScorbotServer, is it running?')
    warning('Unable to initialize.');
    return
end
fprintf('SUCCESS\n');
fprintf('\t"POWER" light on the ScorBot Control Box should be green.\n');


fprintf('\n');
fprintf('Defining default waypoint vector...');
isCreated = ScorCreateVector('USNA',1000);
if ~isCreated
    confirm = false;
    fprintf('FAILED\n');
    warning('Unable to create waypoint vector location.');
    return
end

fprintf('SUCCESS\n\n');
fprintf('Initialization complete.\n');
fprintf('\t(1) Turn teach pendant to AUTO\n');
fprintf('\t(2) Home the ScorBot using "ScorHome"\n')
fprintf('\n'); % add line for cleaner display in the command prompt


%% Confirm and set hidden figure to green
confirm = true;
set(ShutdownFig,'Color',[0.0,1.0,0.0]);
