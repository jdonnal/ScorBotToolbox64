function pMode = ScorGetPendantMode()
% SCORGETPENDANTMODE gets the current mode of the ScorBot teach pendant
%   pMode = SCORGETPENDANTMODE gets the current mode of the ScorBot teach
%   pendant (either "Teach" or "Auto").
%
%   See also: ScorSetPendantMode
%
%   M. Kutzer, 10Aug2015, USNA

% Updates
%   28Aug2015 - Updated error handling
%   25Sep2015 - Ignore isReady flag
%   19Jun2017 - Convert to scorbot server J.Donnal


%% Get teach pendant mode
isTeach = ScorServerCmd('RIsTeach');
switch isTeach
    case 0
        pMode = 'Auto';
    case 1
        pMode = 'Teach';
    otherwise
        error('Unexpected response from ScorServerCmd');
end
